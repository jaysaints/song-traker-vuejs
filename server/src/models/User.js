const { DataTypes } = require('sequelize');
const sequelize = require('./index').sequelize;

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        unique: true
    },
    hash: DataTypes.STRING,
    salt: DataTypes.STRING,
    role: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    }
})

//console.log(User === sequelize.models.User);

module.exports = User;
