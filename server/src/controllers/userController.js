const User = require('../models/User');
const tools = require('../config/tools');

module.exports = {
    async register_post (req, res, next) {
        try {
            const saltHash = tools.genPassword(req.body.password);
            const salt = saltHash.salt;
            const hash = saltHash.hash;

            const user = await User.create({
                email: req.body.email,
                hash: hash,
                salt: salt
            });
            res.status(200).send({success: true, msg: 'OK, User registred!'});
        } catch (err) {
            res.status(400).send({
                error: 'This email account is already in use.'
            })
        }
    },

    async login_post (req, res, next) {
        try {
            const {email, password} = req.body
            await User.findOne({
                where: {
                    email: email
                }          
            }).then(user => {
                if(!user) {
                    res.status(403).send({ error: 'The email information was incorrect!' })
                }

                // Verify password match with hash password!
                const isValidPassword = tools.validPassword(password, user.hash, user.salt);                
                if(!isValidPassword) {
                    res.status(403).send({
                        error: 'The login information was incorrect!'
                    })
                }
                
                // Result match User
                const userJson = user.toJSON();

                // // generate signature Json Web Token
                const signatureJWT = tools.issueJWT(userJson);
                                
                // Send Info User and Signature for client request
                res.send({
                        msg: 'Success Login!',
                        success: true,
                        user: userJson,
                        token: signatureJWT
                    });  
            }); 

        } catch (err) {
            res.status(403).send({
                error: 'An error has occured trying to log in!!'
            })
        }
    }

}