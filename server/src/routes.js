// Require controllers
const userController = require('./controllers/userController');
const songsController = require('./controllers/songsController');

// Valid token signature
const authToken = require('../src/config/tools').authMiddleware;

// Valid fields 
const authPolicy = require('../src/policies/authControllerPolicy');

module.exports = (app) => {
    // Index Page
    app.get('/', (req, res, next) => {
        res.send('<h1>Home Page</h1>')
    })

    // Login Page
    app.post('/login', userController.login_post)

    // Register Page
    app.post('/register', authPolicy.register, userController.register_post)

    // Songs Service Page
    app.get('/songs', songsController.getAllSongs_get)

    // Register new song
    app.post('/songs/add', songsController.registerSong_post)

    // Get information about one song
    app.get('/songs/:songId', songsController.getOneSong_get)

     // Edit information about one song
     app.put('/songs/:songId/edit', songsController.editSong_put)

}




