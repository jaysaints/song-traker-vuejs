import axios from 'axios'

const port = 8081
const domainServe = `http://localhost:${port}`

export default {
  /* Create new account */
  registerUser (payload) {
    return axios.post(`${domainServe}/register`, payload)
  },
  /* Login */
  login (payload) {
    return axios.post(`${domainServe}/login`, payload)
  },
  /* Get all songs in database - POST */
  getAllSongs (search) {
    return axios.get(`${domainServe}/songs`, {
      params: {
        search: search
      }
    })
  },
  /* Register new song - POST */
  registerSongs (payload) {
    return axios.post(`${domainServe}/songs/add`, payload)
  },
  /* Show one song information (http://localhost:8081/songs/092384091384) */
  viewSong (songId) {
    return axios.get(`${domainServe}/songs/${songId}`)
  },
  /* Edit song */
  editSong (songId, payload) {
    return axios.put(`${domainServe}/songs/${songId}/edit`, payload)
  }
}
