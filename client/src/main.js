import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { sync } from 'vuex-router-sync'
import store from './store/store'
import vuetify from './plugins/vuetify'
import axios from 'axios'
import VueYouTubeEmbed from 'vue-youtube-embed'
import Panel from '@/components/global/Panel.vue'

/* Globals Components */
Vue.component('panel', Panel)

Vue.config.productionTip = false

/* Set token in LocalStorage Browser */
const token = localStorage.getItem('token')

/* Set axios in property $http */
Vue.prototype.$http = axios

if (token) {
  Vue.prototype.$http.defaults.headers.common.Authorization = token
}

// Store token, user information and router information
sync(store, router)

Vue.use(VueYouTubeEmbed)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
